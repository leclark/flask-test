# Flask-Test

## Set up local development environment

1. Install Python 3.8 and virtualenv
2. Change to the directory of your local clone of this repository

        cd ~/work/flask-test
3. Create python virtual environment

        python3 -m venv venv
4. Activate python virtual environment

        source env/bin/activate
5. Install required packages

        pip install -r requirements.txt

## Run locally

         source env/bin/activate
         FLASK_APP=app.app flask run

## Docker

    docker build -t flask-test .
    docker run -it --name flask-test --rm -p 5000:5000 \
      flask-test

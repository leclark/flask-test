#!/usr/bin/env python3
from flask import Flask
from time import time

app = Flask(__name__)


@app.route("/healthz")
def health():
    return {
        "status": "OK",
        "timestamp": time()
    }


@app.route("/int/sum/<int:first>/<int:second>")
def int_sum(first: int, second: int):
    return {"sum": first+second}


if __name__ == "__main__":
    app.run()
